created: 20220725065933981
modified: 20220725082554757
tags: 
title: Orange Factory
type: text/vnd.tiddlywiki

An abandoned industrial complex (mostly underground except for the entrances) with lots of automatically generated sections. Seemingly uninhabited, but with subtle traces of human activity here and there, as well as signs of beings more powerful and sinister. 
<br/>

!!!Concept
It has a somewhat eerie atmosphere, but it doesn't try to be a horror game location; that would clash with the glitcher concept. Instead, it is also a setting of awe and wonder, like an underground Laputa. The geometry of the factory is inconsistent, but the ambient grinding and shifting sound effects attempt to explain this.

!!!Gameplay
The Factory consists of an infinite amount of randomly generated floors. These are constructed of rooms, which can also have variations. Some rooms have enemies, some have bosses, some have items, and some have nothing. Each floor will have at least one unlocked elevator or staircase to another level. Some floors may have special exits only unlockable after exploration in other areas.

!!!Lore
The folklore surrounding it is as follows:

<<<
The [[Builders]] had great passion, if not purpose, and within the Machine they built many wonderous things. But as it grew larger and larger, the Machine grew too complex to navigate or repair, a vast labyrinth with flaws so deep and primordial that they became gods themselves. Maybe the Builders could have fixed it, and some tried. But disgusted by their creation, many abandoned it, some later than others. Some still remain, holding on to the First Creation.
<<<